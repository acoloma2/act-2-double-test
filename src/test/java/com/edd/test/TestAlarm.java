package com.edd.test;

import org.junit.*;
import org.junit.jupiter.api.Assertions;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class TestAlarm {

    /**
     * Comprobar que la alarma está desactivada por defecto
     */
    @Test
    public void testAlarmIsNotOnByDefault() {

        Alarm alarm = new Alarm(new Sensor());
        Assertions.assertFalse(alarm.isAlarmOn(), "Alarm is not off by default");

    }
    /**
     * Comprobar que la alarma se activa en con bajas presiones
     */
    @Test
    public void testAlarmOnWithLowPressure() {

        Sensor sensorStub = mock(Sensor.class);
        when(sensorStub.popNextPressurePsiValue()).thenReturn(Alarm.LOW_PRESSURE_THRESHOLD-1);
        Alarm alarm = new Alarm(sensorStub);
        alarm.check();
        Assertions.assertTrue(alarm.isAlarmOn(), "Alarm is not active with low pressure");

    }

    /**
     * Comprobar que la alarma se activa en con altas presiones
     */
    @Test
    public void testAlarmOnWithHighPressure() {

        Sensor sensorStub = mock(Sensor.class);
        when(sensorStub.popNextPressurePsiValue()).thenReturn(Alarm.HIGH_PRESSURE_THRESHOLD+1);
        Alarm alarm = new Alarm(sensorStub);
        alarm.check();
        Assertions.assertTrue(alarm.isAlarmOn(), "Alarm is not active with high pressure");

    }

    /**
     * Comprobar que la alarma permanece desactivda con presiones normales
     */
    @Test
    public void testAlarmOffWithNormalPressure() {

        Sensor sensorStub = mock(Sensor.class);
        when(sensorStub.popNextPressurePsiValue()).thenReturn(Alarm.LOW_PRESSURE_THRESHOLD+2);
        Alarm alarm = new Alarm(sensorStub);
        alarm.check();
        Assertions.assertFalse(alarm.isAlarmOn(), "Alarm is not off within normal range");

    }

    /**
     * Comprobar que la alarma permanece desactivda con presiones límite
     */
    @Test
    public void testAlarmOffWithLimitsPressure() {

        Sensor sensorStub = mock(Sensor.class);
        when(sensorStub.popNextPressurePsiValue()).thenReturn(Alarm.HIGH_PRESSURE_THRESHOLD-0.01);
        Alarm alarm = new Alarm(sensorStub);
        alarm.check();
        Assertions.assertFalse(alarm.isAlarmOn(), "Alarm is not off within high limit pressure");

        alarm.check();
        when(sensorStub.popNextPressurePsiValue()).thenReturn(Alarm.LOW_PRESSURE_THRESHOLD+0.01);
        Assertions.assertFalse(alarm.isAlarmOn(), "Alarm is not off within low limit pressure");
    }

    /**
     * Comprobar que la alarma se activa con presiones límite por encima y por debajo
     */
    @Test
    public void testAlarmOnWithLimitsPressure() {

        Sensor sensorStub = mock(Sensor.class);
        when(sensorStub.popNextPressurePsiValue()).thenReturn(Alarm.HIGH_PRESSURE_THRESHOLD+0.01);
        Alarm alarm = new Alarm(sensorStub);
        alarm.check();
        Assertions.assertTrue(alarm.isAlarmOn(), "Alarm is not ON within high limit pressure");

        alarm.check();
        when(sensorStub.popNextPressurePsiValue()).thenReturn(Alarm.LOW_PRESSURE_THRESHOLD-0.01);
        Assertions.assertTrue(alarm.isAlarmOn(), "Alarm is not ON within low limit pressure");

    }

}
